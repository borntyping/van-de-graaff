#!/usr/bin/env python3

import abc
import argparse
import collections
import datetime
import fnmatch
import os
import os.path
import shutil

class Renderer(object):
    __metaclass__ = abc.ABCMeta

    @classmethod
    def name(cls):
        return cls.__name__.lower().rstrip('Renderer')

    @abc.abstractmethod
    def render(self, path, build_path):
        """This should explicitly return False if no action was taken"""
        pass

class StaticRenderer(Renderer):
    def render(self, path, build_path):
        """Only render the file if it does not exist or has been modified"""
        if os.path.exists(build_path):
            if not os.path.getmtime(path) > os.path.getmtime(build_path):
                return False
        shutil.copy(path, build_path)

class Jinja2Renderer(Renderer):
    def __init__(self, vdg):
        import jinja2

        self.vdg = vdg
        self.enviroment = jinja2.Environment(
            loader=jinja2.FileSystemLoader(vdg.path))
    
    def render(self, path, build_path):
        relative_path = self.vdg.get_relative_path(path)
        template = self.enviroment.get_template(relative_path)
        
        with open(build_path, 'w') as file:
            file.write(template.render(filename=relative_path))

class VanDeGraaff(object):
    ignored_directories = ('.git', 'build', 'templates')
    
    def __init__(self, path, build_path, quiet=False):
        self.path = os.path.abspath(path)
        self.build_path = os.path.abspath(build_path)

        self.renderers = collections.OrderedDict()
        self.default_renderer = StaticRenderer()

        self.quiet = quiet

    def get_relative_path(self, *paths):
        """Returns a path relative to the original folder"""
        return os.path.relpath(os.path.join(*paths), self.path)

    def get_original_path(self, relative_path):
        """Returns the full path of a file in the original folder"""
        return os.path.join(self.path, relative_path)

    def get_build_path(self, relative_path):
        """Returns the full path of a file in the build folder"""
        return os.path.join(self.build_path, relative_path)

    # ---------------------
    # Render multiple files
    # ---------------------

    def render_all(self):
        self.ensure_directory(self.build_path)

        for dirpath, dirnames, filenames in os.walk(self.path):
            # Ignore certain directories
            self.filter_directory_names(dirnames)
            
            for dirname in dirnames:
                # Turn the full path of the directory into a relative path
                relative_path = self.get_relative_path(dirpath, dirname)
                
                # Ensure the directory exists in the build folder
                self.ensure_directory(self.get_build_path(relative_path))
            
            for filename in filenames:
                self.render(self.get_relative_path(dirpath, filename))

    def ensure_directory(self, dirpath):
        """Create a directory at the specified path if it does not exist"""
        if not os.path.exists(dirpath):
            os.mkdir(dirpath)

        if not os.path.isdir(dirpath):
            raise Exception("{} is not a directory".format(dirpath))

    def filter_directory_names(self, dirnames):
        """Remove names of directories that should not be visited"""
        for name in self.ignored_directories:
            if name in dirnames:
                dirnames.remove(name)

    # --------------------
    # Render a single file
    # --------------------

    def render_one(self, path):
        """Render a single file, given a path to it"""
        self.ensure_directory(self.build_path)
        self.render(self.get_relative_path(path))

    def render(self, filepath):
        renderer = self.get_renderer(filepath)
        path = self.get_original_path(filepath)
        build_path = self.get_build_path(filepath)
        if renderer.render(path, build_path) is not False:
            self.log(renderer.name(), filepath)

    def get_renderer(self, filename):
        """Return the renderer that should be used for the given file"""
        for pattern in self.renderers:
            if fnmatch.fnmatch(filename, pattern):
                return self.renderers[pattern]
        return self.default_renderer

    # ----------------------
    # Command line interface
    # ----------------------

    def log(self, category, messsage):
        """Print a message, respecting self.quiet"""
        if not self.quiet:
            time = datetime.datetime.now().strftime("%H:%M:%S")
            print("{} [{:6}] {}".format(time, category, messsage))

    @classmethod
    def main(cls):
        parser = argparse.ArgumentParser()
        parser.add_argument('-d', '--directory', default=None)
        parser.add_argument('-b', '--build-path', default=None)
        parser.add_argument('-q', '--quiet', action='store_true')
        parser.add_argument('path', nargs='?', default=None)
        parser.add_argument('-v', '--version', action='version',
            version='VanDeGraaff 1.0')

        options = parser.parse_args()

        if options.directory is None:
            options.directory = os.getcwd()

        if options.build_path is None:
            options.build_path = os.path.join(options.directory, 'build')

        vdg = cls(options.directory, options.build_path, options.quiet)

        if options.path is not None:
            vdg.render_one(options.path)
        else:
            vdg.render_all()

class DefaultVanDeGraaff(VanDeGraaff):
    """A default implementation using the Static and Jinja2 renderers"""
    
    def __init__(self, *args, **kwargs):
        super(DefaultVanDeGraaff, self).__init__(*args, **kwargs)
        self.renderers['*.html'] = Jinja2Renderer(self)

if __name__ == '__main__':
    DefaultVanDeGraaff.main()
