Van De Graaff
=============

Van De Graaff is a static site generator, written in python.
It is designed to be as simple as possible, so that it can be used to output a site using any combination of renderers.

It does this by matching `filename patterns <http://docs.python.org/2/library/fnmatch.html>`_ to renderers.
As an example, the default implementation uses two renderers. Files matching ``*.html`` are rendered with the `jinja2 templating engine <http://jinja.pocoo.org/>`_.
All other files are rendered with a default renderer, which copies files to the build directory if they are newer than the previously copied version (or if the file does not exist in the build directory).

Adding renderers
----------------

This is an example of a script that would run the generator with a custom renderer.

.. code-block:: python

    from vdg import Renderer, VanDeGraaff
    
    class MyStaticRenderer(Renderer):
        """A simplified version of the inbuilt static render

        Renderer is an abstract base class, and is not strictly necessary"""
        
        def render(self, path, build_path):
            shutil.copy(path, build_path)

    class MyVanDeGraaff(VanDeGraaff):
        """An example subclass using MyStaticRenderer"""

        def __init__(self, *args, **kwargs):
            super(MyVanDeGraaff, self).__init__(*args, **kwargs)
            self.renderers['*.txt'] = MyStaticRenderer()

    if __name__ == '__main__':
        MyVanDeGraaff.main()

Licence
-------

Copyright (c) 2013 Sam Clements (borntyping)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
